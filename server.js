#!/usr/bin/env node
"use strict";
const fs = require('fs');
const express = require('express');
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser');

let app = express();

let config = {
  port: 3000,
  endpoint: "localhost",
  debug: false
}

app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static('./public'));

let server = app.listen(config.port, function () {
  console.log('SocketNet listening on port ' + config.port + '!')
})

let lobby = require("./lobby.js").Lobby(server, config);
