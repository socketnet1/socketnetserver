"use strict";
const { v4: uuid } = require("uuid");
const Types = require("./public/js/base.js").Types;

var config = null;

exports.Session = function(websocket, data) {
  return new Session(websocket, data);
}
exports.Init = function (cfg) {
  config = cfg;
}

class Session {
  constructor(ws, data) {
    this.sockets = [ws];
    this.id = uuid();
    this.data = data;
    ws.on("close", () => this.onSocketClose(ws));
  }
  onSocketClose(ws){
    var index = this.sockets.indexOf(ws);
    if (index > -1) this.sockets.splice(index, 1);
    if (this.sockets.length == 0){
      this.onClose(this);
    }
    this.onSocketLeft(ws);
  }
  onSocketLeft(socket) {

  }
  onClose (session) {

  }
  send(msg){
    for (let n in this.sockets){
      let socket = this.sockets[n];
      if (socket.id != msg.sender) socket.send(JSON.stringify(msg));
    }
  }
  join(socket) {
    if (!this.sockets.some(s => s.id === socket.id)) {
      this.sockets.push(socket);
      socket.on("close", () => this.onSocketClose(socket));
      return true;
    }
    return false;
  }
  serialize() {
    var data = {
      id: this.id,
      sockets: [],
      data: this.data
    }
    //console.log(data);
    for (let socket in this.sockets){
      data.sockets.push(this.sockets[socket].id);
    }
    return data;
  }
}
