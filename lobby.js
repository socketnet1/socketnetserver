"use strict";
const WebSocketServer = require('ws').Server;
const Sessions = require("./sessions.js");
const { v4: uuid } = require("uuid");
const Types = require("./public/js/base.js").Types;

exports.Lobby = function(server, config) {
  Sessions.Init(config);
  return new Lobby(server, config);
}

class Lobby {
  constructor(server, config) {
    this.server = server;
    this.config = config;
    this.sockets = {};
    this.sessions = {};
    this.wss = new WebSocketServer({
      server: server,
      path: "/"
    });
    this.wss.on("connection", (ws) => this.onConnection(ws));
  }
  onConnection(ws) {
    ws.on("message", (msg) => { this.onMessage(ws, msg) });
    ws.id = uuid();
    let msg = {
      type: Types.Handshake,
      id: ws.id
    }
    //console.log(msg)
    ws.send(JSON.stringify(msg));
  }
  onMessage(ws, data){
    try {
      var msg = JSON.parse(data);
      msg.sender = ws.id;
      //console.log(msg);
      switch (msg.type) {
        case Types.Error:
          this.handleError(ws, msg);
          break;
        case Types.Handshake:
          this.handleHandshake(ws, msg);
          break;
        case Types.Broadcast:
          this.handleBroadcast(msg);
          break;
        case Types.SendToID:
          this.handleSendToID(ws, msg);
          break;
        case Types.SendToSession:
          this.handleSendToSession(ws, msg);
          break;
        case Types.JoinSession:
          this.handleJoinSession(ws, msg);
          break;
        case Types.CreateSession:
          this.handleCreateSession(ws, msg);
          break;
        default:
          ws.send(JSON.stringify({ type: Types.Error, error: "message type supported" }));
      }
    } catch (e){
      console.log(e);
      ws.send(JSON.stringify({ type: Types.Error, error: "message not supported" }));
    }
  }
  onClose(sender){
    //console.log("ws closed: " + sender.id);
    delete this.sockets[sender.id];
    this.handleBroadcast({ type: Types.SocketClosed, id: sender.id });
  }
  handleError(sender, msg){
    console.log(msg);
  }
  handleBroadcast(msg){
    for (var id in this.sockets){
      var socket = this.sockets[id];
      socket.send(JSON.stringify(msg));
    }
  }
  handleSendToID(sender, msg){
    if (msg.id in this.sockets) this.sockets[msg.id].send(JSON.stringify(msg));
  }
  handleSendToSession(sender, msg){
    if (msg.id in this.sessions){
      this.sessions[msg.id].send(msg);
    }
  }
  handleJoinSession(sender, msg){
    if (msg.id in this.sessions){
      let session = this.sessions[msg.id];
      if (session.join(sender)) {
        var msg = {
          type: Types.SocketJoinedSession,
          id: session.id,
          sender: sender.id
        }
        this.handleBroadcast(msg);
      }
    }
  }
  handleCreateSession(sender, msg){
    //console.log(msg.data);
    var session = new Sessions.Session(sender, msg.data);
    session.onClose = () => this.onSessionClosed(session);
    session.onSocketLeft = (socket) => this.onSocketLeftSession(socket, session);
    this.sessions[session.id] = session;
    var msg = {
      sender: sender.id,
      type: Types.SessionOpened,
      data: session.serialize()
    }
    this.handleBroadcast(msg);
  }
  onSessionClosed(session){
    if (session.id in this.sessions){
      delete this.sessions[session.id];
    }
    this.handleBroadcast({ type: Types.SessionClosed, id: session.id });
  }
  onSocketLeftSession(socket, session) {
    var msg = {
      type: Types.SocketLeftSession,
      sender: socket.id,
      id: session.id
    }
    this.handleBroadcast(msg);
  }
  handleHandshake(sender, msg){
    //console.log(msg);
    sender.username = msg.username;
    var somsg = { type: Types.SocketOpened, id: sender.id, username: sender.username };
    this.handleBroadcast(somsg);
    this.sockets[sender.id] = sender;
    sender.on("close", () => this.onClose(sender));
    sender.send(JSON.stringify(this.serialize()));
  }
  getSockets(){
    var sockets = {};
    for (var id in this.sockets) {
      sockets[id] = { id: id, username: this.sockets[id].username };
    }
    return sockets;
  }
  getSessions(){
    var sessions = {};
    for (let id in this.sessions){
      sessions[id] = this.sessions[id].serialize();
    }
    return sessions;
  }
  serialize() {
    let data = {
      data: {
        sockets: this.getSockets(),
        sessions: this.getSessions(),
      },
      type: Types.ServerState
    }
    return data;
  }
}
