"use strict";
const Types = {
  Error: 0,
  Broadcast: 1,
  SendToID: 2,
  SendToSession: 3,
  Handshake: 4,
  SocketClosed: 5,
  SocketOpened: 6,
  JoinSession: 7,
  CreateSession: 8,
  LeaveSession: 9,
  SessionOpened: 10,
  SessionClosed: 11,
  SocketJoinedSession: 12,
  SocketLeftSession: 13,
  ServerState: 14
}
if (typeof module !== "undefined"){
  exports.Types = Types;
}
