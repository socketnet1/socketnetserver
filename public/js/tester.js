"use strict";
document.addEventListener("DOMContentLoaded", function(){
  init();
});

var ws = null;
var sockets = null;
var sessions = null;
var myId = null;

function init(){
  console.log("initializing webglnet tester");
  ws = new WebSocket("ws://localhost:3000");
  ws.onmessage = onMessage;// (msg)=>{ console.log(msg.data); }
  ws.onclose = () => {
    console.log("this socket closed");
    init();
  }
}

async function printBlob(blob){
  var text = await blob.text();
  console.log(text);
}

function onMessage(e) {
  if (e.data instanceof Blob){
    console.log("blob contents:");
    printBlob(e.data);
    return;
  }
  //console.log(e.data);
  var msg = JSON.parse(e.data);
  switch (msg.type){
    case Types.Handshake:
      //console.log("handshake");
      msg.username = localStorage.username ? localStorage.username : "NewUser";
      myId = msg.id;
      ws.send(JSON.stringify(msg));
      break;
    case Types.ServerState:
      sockets = msg.data.sockets;
      sessions = msg.data.sessions;
      for (let id in sessions){
        sessions[id]["info"]= {};
      }
      updateUI();
      break;
    case Types.SocketOpened:
      sockets[msg.id] = { id: msg.id, username: msg.username };
      updateUI();
      break;
    case Types.SocketClosed:
      if (msg.id in sockets) delete sockets[msg.id];
      updateUI();
      break;
    case Types.SessionOpened:
      sessions[msg.data.id] = msg.data;
      msg.data["info"] = {};
      updateUI();
      break;
    case Types.SessionClosed:
      if (msg.id in sessions) delete sessions[msg.id];
      updateUI();
      break;
    case Types.SocketJoinedSession:
      console.log("SocketJoinedSession");
      if (msg.id in sessions){
        let session = sessions[msg.id];
        session.sockets.push(msg.sender);
      }
      updateUI();
      break;
    case Types.SocketLeftSession:
      console.log("SocketLeftSession");
      if (msg.id in sessions){
        let session = sessions[msg.id];
        let index = session.sockets.indexOf(msg.sender);
        sessions[msg.id].sockets.splice(index, 1);// = msg.session;
      }
      updateUI();
      break;
    case Types.SendToSession:
      //console.log(msg);
      handleSessionData(msg);
      break;
    default:
      console.log("unhandled message");
      console.log(msg);
  }
}

function createSession() {
  var msg = {
    type: Types.CreateSession,
    data: {
      name: "Bente",
      type: 0
    }
  }
  ws.send(JSON.stringify(msg));
}

function createSocketElement(socket){
  var socketElement = document.createElement("div");
  socketElement.classList.add("element");
  socketElement.innerHTML = socket.username;
  return socketElement;
}

function updateUI(){
  var sessionsElement = document.getElementById("sessions");
  var socketsElement = document.getElementById("sockets");
  sessionsElement.innerHTML = "";
  socketsElement.innerHTML = "";
  for (let id in sockets){
    let socket = sockets[id];
    let socketElement = createSocketElement(socket);
    socketsElement.appendChild(socketElement);
  }
  for (let id in sessions){
    //console.log(id);
    let session = sessions[id];
    let sessionElement = document.createElement("div");
    sessionElement.classList.add("container");
    //sessionElement.innerHTML = session.username;
    sessionsElement.appendChild(sessionElement);
    let joinButton = document.createElement("button");
    joinButton.innerHTML = "Join";
    joinButton.onclick = () => onJoinSessionClicked(session);
    let sessionSocketsElement = document.createElement("div");
    sessionSocketsElement.classList.add("container");
    let sessionCanvas = document.createElement("canvas");
    sessionCanvas.classList.add("sessionCanvas");
    session.canvas = sessionCanvas;
    sessionCanvas.onmousemove = (e) => onCanvasMouseMove(e, id);
    sessionElement.appendChild(joinButton);
    sessionElement.appendChild(sessionSocketsElement);
    sessionElement.appendChild(sessionCanvas);

    for (let n in session.sockets){
      let socketId = session.sockets[n];
      if (socketId in sockets){
        let socket = sockets[socketId];
        let socketElement = createSocketElement(socket);
        sessionSocketsElement.appendChild(socketElement);
      }
    }
  }
}

function onJoinSessionClicked(session){
  let msg = {
    type: Types.JoinSession,
    id: session.id
  }
  ws.send(JSON.stringify(msg));
}

function onCanvasMouseMove(e, id) {
  let msg = {
    type: Types.SendToSession,
    id: id,
    data: { x: e.offsetX, y: e.offsetY }
  }
  ws.send(JSON.stringify(msg));
}

function handleSessionData(msg) {
  if (msg.id in sessions){
    console.log(msg);
    let session = sessions[msg.id];
    session.info[msg.sender] = msg.pos;
    drawCanvas(session);
  }
}

function drawCanvas(session){
  let canvas = sessions.canvas;

}
